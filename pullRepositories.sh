#!/bin/bash

# Move to $ROS_ROOT
source `rospack find rosbash`/rosbash
roscd

# Determine the longest directory name
maxlen=0
for dir in *
do
    if [[ -d $dir ]]
    then
        curlen=${#dir}
        if [[ $curlen -gt $maxlen ]]
        then
            maxlen=$curlen
        fi
    fi
done

# For each directory, if the branch exists then switch to it
for dir in *
do
    if [[ -d $dir ]]
    then
        cd $dir
        printf "%-${maxlen}s: " $dir
        git pull
        cd ..
    fi
done
