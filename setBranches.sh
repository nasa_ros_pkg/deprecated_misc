#!/bin/bash

if [[ $# -ne 1 ]]
then
    echo "Usage: `basename $0` <branchname>"
fi
desired=$1

# Move to $ROS_ROOT
source `rospack find rosbash`/rosbash
roscd

# Determine the longest directory name
maxlen=0
for dir in *
do
    if [[ -d $dir ]]
    then
        curlen=${#dir}
        if [[ $curlen -gt $maxlen ]]
        then
            maxlen=$curlen
        fi
    fi
done

# For each directory, if the branch exists then switch to it
for dir in *
do
    if [[ -d $dir ]]
    then
        cd $dir
        printf "%-${maxlen}s: " $dir
        curbranch=`git rev-parse --abbrev-ref HEAD`
        # If branch exists
        if [[ `git branch -a | grep "$desired" | wc -l` -ne 0 ]]
        then
            # If not already on desired branch
            if [[ $curbranch != $desired ]]
            then
                git checkout -q $desired
                # If the checkout was successful
                if [[ `git rev-parse --abbrev-ref HEAD` == $desired ]]
                then
                    printf "%s -> %s\n" $curbranch $desired
                else
                    printf "%-${maxlen}s: %s\n" $dir $curbranch
                fi
            else
                printf "%s\n" $curbranch
            fi
        else
            printf "%s\n" $curbranch
        fi
        cd ..
    fi
done
