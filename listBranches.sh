#!/bin/bash

function printHelp()
{
    echo "Usage: `basename $0` [-a] [-h] [branch]"
    echo "Options:"
    echo "    -a      Display all branches for each repository in the current directory"
    echo "    -h      Display this help message"
    echo "    branch  List only the repositories in the current directory that are on the given branch"
    exit
}

# Get optional arguments
listall="false"
while getopts ":ah" Option
do
  case $Option in
    a ) listall="true";;
    h ) printHelp;;
    * ) printHelp;;   # Default
  esac
done
shift $(($OPTIND - 1))

# Move to $ROS_ROOT
source `rospack find rosbash`/rosbash
roscd

# Determine the longest directory name
maxlen=0
for dir in *
do
    if [[ -d $dir ]]
    then
        curlen=${#dir}
        if [[ $curlen -gt $maxlen ]]
        then
            maxlen=$curlen
        fi
    fi
done

# Print the current branch of each directory
for dir in *
do
    if [[ -d $dir ]]
    then
        cd $dir
        if [[ "$listall" == "true" ]]
        then
            printf "%-${maxlen}s\n--------------\n" $dir
            git branch -a
            echo ""
        else
            branchname=`git rev-parse --abbrev-ref HEAD`
            if [[ $# -eq 1 ]]
            then
                if [[ "$branchname" == "$1" ]]
                then
                    printf "%-${maxlen}s: " $dir
                    printf "%s\n" $branchname
                fi
            else
                printf "%-${maxlen}s: " $dir
                printf "%s\n" $branchname
            fi
        fi
        cd ..
    fi
done
